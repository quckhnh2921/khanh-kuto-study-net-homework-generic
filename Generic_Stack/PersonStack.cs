﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    public class PersonStack : MyStack<Person>
    {
        Person[] personStack;
        int top = -1;
        public PersonStack(int maxStack) : base(maxStack)
        {
            personStack = new Person[maxStack];
        }

        protected override Person[] myStack 
        { 
            get => personStack;
            set => personStack = value; 
        }

        public override void Clear()
        {
            top = -1;
        }

        public override bool IsEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            return false;
        }

        public override bool IsFull()
        {
            if (top == myStack.Length)
            {
                return true;
            }
            return false;
        }

        public override Person Peek()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            var valueAtTop = myStack[top];
            top--;
            return valueAtTop;
        }

        public override Person Pop()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            return myStack[top];
        }

        public override void Push(Person item)
        {
            top++;
            if (IsFull() is true)
            {
                top--;
                throw new Exception("Stack is full");
            }
            myStack[top] = item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    public class Menu
    {
        public void MainMenu()
        {
            Console.WriteLine("==========Menu==========");
            Console.WriteLine("| 1. Integer Stack     |");
            Console.WriteLine("| 2. String Stack      |");
            Console.WriteLine("| 3. Person Stack      |");
            Console.WriteLine("| 4. Exit              |");
            Console.WriteLine("========================");
            Console.Write("User choose: ");
        }

        public void MenuStack()
        {
            Console.WriteLine("==========Menu==========");
            Console.WriteLine("| 1. Push              |");
            Console.WriteLine("| 2. Pop               |");
            Console.WriteLine("| 3. Peak              |");
            Console.WriteLine("| 4. Clear             |");
            Console.WriteLine("| 5. Exit              |");
            Console.WriteLine("========================");
            Console.Write("User choose: ");
        }
    }
}

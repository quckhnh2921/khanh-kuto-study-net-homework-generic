﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    public class IntegerStack : MyStack<int>
    {
        int[] integerStacks;
        int top = -1;
        public IntegerStack(int maxStack) : base(maxStack)
        {
            integerStacks = new int[maxStack];
        }

        protected override int[] myStack
        {
            get => integerStacks;
            set => integerStacks = value;
        }

        public override void Clear()
        {
            top = -1;
        }

        public override bool IsEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            return false;
        }

        public override bool IsFull()
        {
            if (top == myStack.Length)
            {
                return true;
            }
            return false;
        }

        public override int Peek()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            return myStack[top];
        }

        public override int Pop()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            var valueAtTop = myStack[top];
            top--;
            return valueAtTop;
        }

        public override void Push(int item)
        {
            top++;
            if (IsFull() is true)
            {
                top--;
                throw new Exception("Stack is full");
            }
            myStack[top] = item;
        }
    }
}

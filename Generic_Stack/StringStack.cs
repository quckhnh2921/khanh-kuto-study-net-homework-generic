﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    public class StringStack : MyStack<string>
    {
        string[] stringStacks;
        int top = -1;
        public StringStack(int maxStack) : base(maxStack)
        {
            stringStacks = new string[maxStack];
        }

        protected override string[] myStack 
        { 
            get => stringStacks; 
            set => stringStacks = value; 
        }

        public override void Clear()
        {
            top = -1;
        }

        public override bool IsEmpty()
        {
            if (top == -1)
            {
                return true;
            }
            return false;
        }

        public override bool IsFull()
        {
            if (top == myStack.Length)
            {
                return true;
            }
            return false;
        }

        public override string Peek()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            return myStack[top];
        }

        public override string Pop()
        {
            if (IsEmpty() is true)
            {
                throw new Exception("Empty stack");
            }
            var valueAtTop = myStack[top];
            top--;
            return valueAtTop;
        }

        public override void Push(string item)
        {
            top++;
            if (IsFull() is true)
            {
                top--;
                throw new Exception("Stack is full");
            }
            myStack[top] = item;
        }
    }
}

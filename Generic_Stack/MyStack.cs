﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Stack
{
    public abstract class MyStack<T>
    {
        protected abstract T[] myStack { get; set; }
        public MyStack(int maxStack)
        {
            myStack = new T[maxStack];
        }
        public abstract void Push(T item);
        public abstract T Pop();
        public abstract T Peek();
        public abstract bool IsEmpty();
        public abstract bool IsFull();
        public abstract void Clear();
    }
}

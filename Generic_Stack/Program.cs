﻿using Generic_Stack;
Menu menu = new Menu();
MyStack<int> integerStack = new IntegerStack(5);
MyStack<string> stringStack = new StringStack(5);
MyStack<Person> personStack = new PersonStack(5);
int choice = 0;
do
{
    try
    {
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                int choiceIntegerStack = 0;
                do
                {
                    try
                    {
                        menu.MenuStack();
                        choiceIntegerStack = int.Parse(Console.ReadLine());
                        switch (choiceIntegerStack)
                        {
                            case 1:
                                Console.WriteLine("Enter integer to push");
                                int item = int.Parse(Console.ReadLine());
                                integerStack.Push(item);
                                break;
                            case 2:
                                Console.WriteLine("Pop: " + integerStack.Pop());
                                break;
                            case 3:
                                Console.WriteLine("Peek: " + integerStack.Peek());
                                break;
                            case 4:
                                integerStack.Clear();
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (choiceIntegerStack < 5);
                break;
            case 2:
                int choiceStringStack = 0;
                do
                {
                    try
                    {
                        menu.MenuStack();
                        choiceStringStack = int.Parse(Console.ReadLine());
                        switch (choiceStringStack)
                        {
                            case 1:
                                Console.WriteLine("Enter string to push");
                                string item = Console.ReadLine();
                                stringStack.Push(item);
                                break;
                            case 2:
                                Console.WriteLine("Pop: " + stringStack.Pop());
                                break;
                            case 3:
                                Console.WriteLine("Peek: " + stringStack.Peek());
                                break;
                            case 4:
                                stringStack.Clear();
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (choiceStringStack < 5);
                break;
            case 3:
                int choicePersonStack = 0;
                do
                {
                    try
                    {
                        menu.MenuStack();
                        choicePersonStack = int.Parse(Console.ReadLine());
                        switch (choicePersonStack)
                        {
                            case 1:
                                Person person = new Person();
                                Console.WriteLine("Enter name to push");
                                person.Name = Console.ReadLine();
                                Console.WriteLine("Enter phone number to push");
                                person.PhoneNumber = int.Parse(Console.ReadLine());
                                personStack.Push(person);
                                break;
                            case 2:
                                var personPop = personStack.Pop();
                                Console.WriteLine($"Name: {personPop.Name} | Phone: {personPop.PhoneNumber}");
                                break;
                            case 3:
                                var personPeek = personStack.Peek();
                                Console.WriteLine($"Name: {personPeek.Name} | Phone: {personPeek.PhoneNumber}");
                                break;
                            case 4:
                                personStack.Clear();
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (choicePersonStack < 5);
                break;
            case 4:
                Console.WriteLine("Bye bye");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Please choose from 1 to 4");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);



﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_Queue
{
    public class MyQueue<T>
    {
        T[] items;
        int FirstItemIdex = -1;
        int bot = -1;

        public MyQueue(int maxQueue)
        {
            items = new T[maxQueue];
            FirstItemIdex = 0;
        }

        public void Enqueue(T item)
        {
            if(bot == items.Length)
            {
                throw new Exception("Queue is full");
            }
            bot++;
            items[bot] = item;
        }

        public T Dequeue()
        {
            var valueAtTop = items[FirstItemIdex];
            FirstItemIdex++;
            return valueAtTop;
        }

        public T Peek()
        {
            return items[FirstItemIdex];
        }

        public void Clear()
        {
            bot = -1;
            FirstItemIdex = -1;
        }

        public int Count()
        {
            int count = 0;
            for (int i = 0; i < bot; i++)
            {
                count += 1;
            }
            return count;
        }

        public bool IsEmpty()
        {
            if(bot == -1)
            {
                return true;
            }
            return false;
        }

        public bool IsFull()
        {
            if(bot == items.Length)
            {
                return true;
            }
            return false;
        }
    }
}

﻿using Generic_Queue;

var stringQueue = new MyQueue<string>(5);
Menu menu = new Menu();
int choice = 0;
do
{
    menu.MenuQueue();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            stringQueue.Enqueue(Console.ReadLine());
            break;
        case 2:
            stringQueue.Dequeue();
            break;
        case 3:
            stringQueue.Peek();
            break;
        case 4:
            stringQueue.Count();
            break;
        case 5:
            Console.WriteLine("Bye bye");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Please choose from 1 to 5");
            break;
    }
} while (choice != 5);